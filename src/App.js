import React, { Component } from 'react';
import './App.css';
import DownloadFile from './Components/DownloadFile/DownloadFile'
import FileDetails from './fileDetails'

class App extends Component {
  render() {
    return (
      <div className="App">
       <DownloadFile fileDetails={FileDetails}/>
      </div>
    );
  }
}

export default App;
