import React from 'react';
import { shallow } from 'enzyme';
import App from './App';

describe("App",()=> {
  it("renders without crashing", () => {
    let wrapper = shallow(<App/>)
    expect(wrapper.length).toEqual(1);
  });

  it("renders DownloadFile component", () => {
    let wrapper = shallow(<App/>)
    expect(wrapper.text()).toContain("<DownloadFile />")
  })

})


