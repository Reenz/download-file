import React,{ Component }from 'react';
import PropTypes from 'prop-types';
import './DownloadFile.scss'

class DownloadFile extends Component {

  constructor(props){
    super(props);
    this.chkBoxRef = React.createRef();
    this.state = {
      fileDetails: props.fileDetails,
      selectedCount: 0,
      checkedArray: Array(props.fileDetails.length).fill(false),
      selectAll: false
    }
  }

  handleSelectedItem = (event) => {
    let cnt = this.getSelectedCount(event);
    this.handleIndeterminate(cnt);
    this.getCheckedArray(event);
  }

  getSelectedCount = (event) => {
    let oldCount = this.state.selectedCount;
    const newCount = (event.target).checked ? oldCount + 1 : oldCount - 1;
    const updateSelectALL= newCount > 0 ? true : false;
    this.setState({selectedCount: newCount, selectAll:updateSelectALL});
    return newCount;
  }

  getCheckedArray = (event) => {
    const idx = event.target.value;
    const newCheckedArray = this.state.checkedArray;
    newCheckedArray[idx] = event.target.checked;
    this.setState({checkedArray: newCheckedArray})
  }

  handleDownload = () => {
    const checkedArray = this.state.checkedArray;
    let msg = "";
    
    checkedArray.forEach((isChecked,i) => {
      const fileDetails = this.state.fileDetails[i]
      if (isChecked === true && fileDetails.status === "available"){
        msg += ("Device: " + fileDetails.device + "\n"+ "Path: " + fileDetails.path );
      }})
      alert(msg)
  }

  selectAll = () => {
    const fileDetails = this.state.fileDetails; 
    const newCheckedArray = Array(fileDetails.length).fill(true);
    const newUnCheckedArray = Array(fileDetails.length).fill(false);

    if (this.state.selectAll === false || this.state.selectedCount < fileDetails.length){
      this.setState({selectedCount: (newCheckedArray.length), checkedArray : newCheckedArray, selectAll: true})
    } else{
      this.setState({selectedCount: 0,checkedArray : newUnCheckedArray, selectAll: false});
    }    
  }

  handleIndeterminate = (cnt) => {
    const fileDetailsLength = this.state.fileDetails.length;
    (cnt > 0 && cnt < fileDetailsLength)? this.chkBoxRef.current.indeterminate = true: this.chkBoxRef.current.indeterminate = false
  }


  render() {
    const fileDetails = this.state.fileDetails; 
    const checkedArray = this.state.checkedArray;
    
    const files = fileDetails.map((file, index) => (
      <div key={file.name} className={checkedArray[index]=== true ? "TableRow SelectedRow" : "TableRow"} >

        <input type="checkbox" id={"chk" + index} value={index} name={file.name} checked={checkedArray[index]}
          onChange={this.handleSelectedItem}/>

        <div className="TableData">{file.name}</div>
        <div className="TableData">{file.device}</div>
        <div className="TableData">{file.path}</div>
        <div className="TableData">{file.status}</div>
      </div>
    ))
    
    return(
      <div className="TableContainer">

        <div className="TableRow">
          <input type="checkbox" ref={this.chkBoxRef} checked={this.state.selectAll} onChange={this.selectAll} className="SelectAll" />

          <div className="TableData">{this.state.selectedCount=== 0? "None Selected": "Selected "+ this.state.selectedCount}</div>

          <div className="TableData"><input type="button" value="⤓" className="Alert" onClick={this.handleDownload}/> Download Selected</div>

        </div>

        <div className="TableRow">
          <div></div>
          <div className="TableData">Names </div>
          <div className="TableData">Device</div>
          <div className="TableData">Path  </div>
          <div className="TableData">Status</div>
        </div>
        {files} 
      </div>
    )
  }
}

DownloadFile.propTypes = {
  fileDetails: PropTypes.array.isRequired
}

export default DownloadFile;