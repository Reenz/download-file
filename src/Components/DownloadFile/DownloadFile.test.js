import React from 'react';
import { mount } from 'enzyme';
import DownloadFile from './DownloadFile.js';

describe("Download File component",() => {
  let wrapper;
  const fileDetails = [
    { name: 'fileOne.exe',
      device: 'deviceOne',
      path: './path/to/fileOne',
      status: 'scheduled'},
     
    { name: 'fileTwo.exe',
      device: 'deviceTwo',
      path: './path/to/fileTwo',
      status: 'available'}
  ]
  beforeEach(()=>{
    wrapper = mount(<DownloadFile fileDetails={fileDetails}/>);
  })
  
  it("renders state fileDetails as not null", () => {
    expect(wrapper.state('fileDetails')).not.toBe(null);
  })

  it("returns the name of the file", () => {
    fileDetails.map((file) => {
      expect(wrapper.containsMatchingElement(<div>{file.name}</div>)).toBe(true);
    })
  })

  it("return the device information of the file", () => {
    fileDetails.map((file) => {
      expect(wrapper.containsMatchingElement(<div>{file.device}</div>)).toBe(true);
    })
  })

  it("return the path of the file", () => {
    fileDetails.map((file) => {
      expect(wrapper.containsMatchingElement(<div>{file.path}</div>)).toBe(true);
    })
  })

  it("return the status of the file", () => {
    fileDetails.map((file) => {
      expect(wrapper.containsMatchingElement(<div>{file.status}</div>)).toBe(true);
    })
  })

  it("returns 'None Selected' when nothing is checked", () => {
    expect(wrapper.text()).toContain("None Selected");
  })

  it("returns 'Selected 1' when one file is checked", () => {
    const evtValue = {
      target : {
        value: '0',
        name: 'fileOne.exe',
        checked: true
      }
    }
    wrapper.find('#chk0').simulate("change", evtValue);
    expect(wrapper.text()).toContain("Selected 1");
  })
  it("returns alert message on clicking download selected", () => {
    window.alert = jest.fn();
    const evtValue = {
      target : {
        value: '1',
        name: 'fileOne.exe',
        checked: true
      }
    }
    wrapper.find('#chk1').simulate("change", evtValue);
    wrapper.find('.Alert').simulate("click")
    expect(window.alert).toHaveBeenCalledWith("Device: deviceTwo\nPath: ./path/to/fileTwo" );
  })

  it("checks if Clicking the select-all checkbox should select all items",() => {
    wrapper.find(".SelectAll").simulate("change")
    expect(wrapper.text()).toContain("Selected 2")
  })

  it("checks if Clicking the select-all checkbox selects all items when one is already selected",() => {
    const evtValue = {
      target : {
        value: '1',
        name: 'fileOne.exe',
        checked: true
      }
    }
    wrapper.find('#chk1').simulate("change", evtValue);
    wrapper.find(".SelectAll").simulate("change")
    expect(wrapper.text()).toContain("Selected 2")
  })

  it("checks if Clicking the select-all checkbox de-selects all items when all item are selected",() => {
    const evtValue = {
      target : {
        value: '0',
        name: 'fileOne.exe',
        checked: true
      }
    }

    const evtValueTwo = {
      target : {
        value: '1',
        name: 'fileOne.exe',
        checked: true
      }
    }
    wrapper.find('#chk0').simulate("change", evtValue);
    wrapper.find('#chk1').simulate("change", evtValueTwo);
    wrapper.find(".SelectAll").simulate("change")
    expect(wrapper.text()).toContain("None Selected")
  })

  it("checks if select-all checkbox is indeterminate state if some but not all items are selected",() => {
    const evtValue = {
      target : {
        value: '1',
        name: 'fileOne.exe',
        checked: true
      }
    }
    wrapper.find('#chk1').simulate("change", evtValue);
    expect(wrapper.find('.SelectAll').first().getDOMNode().indeterminate).toBeTruthy();
  })

  it("checks if select-all checkbox is not in indeterminate state if all items are selected",() => {
    const evtValue = {
      target : {
        value: '0',
        name: 'fileOne.exe',
        checked: true
      }
    }

    const evtValueTwo = {
      target : {
        value: '1',
        name: 'fileOne.exe',
        checked: true
      }
    }
    wrapper.find('#chk0').simulate("change", evtValue);
    wrapper.find('#chk1').simulate("change", evtValueTwo);
    expect(wrapper.find('.SelectAll').first().getDOMNode().indeterminate).toBeFalsy();
  })


})


