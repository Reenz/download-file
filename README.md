### Download File
This project displays a list of files which can be downloaded.

### Features 
* [✓] Only those that have a status of available are currently able to be downloaded. Your implementation should manage this.
* [✓] The select-all checkbox should be in an unselected state if no items are selected.
* [✓] The select-all checkbox should be in a selected state if all items are selected.
* [✓] The select-all checkbox should be in an indeterminate state if some but not all items are selected.
* [✓] The 'Selected 2' text should reflect the count of selected items and display 'None Selected' when there are none selected.
* [✓] Clicking the select-all checkbox should select all items if none or some are selected.
* [✓] Clicking the select-all checkbox should de-select all items if all are currently selected.
* [ ] Status should be correctly formatted
* [✓] Clicking 'Download Selected' when some or all items are displayed should generate an alert box with the path and device of all selected files.
* [✓] Rows should change colour when selected and on hover.

### Screenshot
![component](./src/Images/download.png)

### Running the App and Tests
* Clone the project `git clone git@bitbucket.org:Reenz/download-file.git`
* `cd download-file`
* Run `npm install` - to install all the dependencies
* Run `npm start` - to start the app and visit localhost:3000
* Run `npm test` - to run the tests

### Technologies
* React
* Jest,Enzyme

### Area of Improvement
* Properly manage the height of the table cells as they shrink and grow relative to its content
* Add background color to the checkbox and select all checkbox
* Add download icon to the download button
* Alert with appropriate message like "Nothing Selected" or "Not available for download"
* Add available status styling
* Write more test to cover more scenarios like "None Selected"  
* Use flexbox to create table 
  